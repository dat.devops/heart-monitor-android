package com.example.dathu.heartmonitor;


import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;

import com.example.dathu.heartmonitor.bluetooth.BluetoothService;
import com.example.dathu.heartmonitor.network.ApiService;

import java.util.UUID;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainApplication extends Application{
    private static final String BASE_URL = "http://192.168.1.122:8080/heartmonitorbackend/api/";
    private static final String BASE_URL_FLUX = "http://192.168.1.122:8086/";
    private UUID Uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static MainApplication instance;
    private Retrofit retrofit;
    private ApiService service;
    private ApiService fluxService;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket socket;
    private BluetoothService bluetoothService;
    private User currentUser;
    private boolean sendDataSetting;
    private boolean emergencySetting;
    private String bufferBluetooth;

    public static MainApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        setBluetoothAdapter();
        service = retrofit.create(ApiService.class);
        fluxService = new Retrofit.Builder()
                .baseUrl(BASE_URL_FLUX)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService.class);
    }

    public Retrofit getRetrofit(){
        return this.retrofit;
    }

    public ApiService getApiService(){
        return this.service;
    }

    public ApiService getFluxService(){
        return fluxService;
    }

    public BluetoothAdapter getBluetoothAdapter() {
        return this.bluetoothAdapter;
    }

    public void setBluetoothAdapter(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public UUID getUuid(){
        return Uuid;
    }

    public void setUuid(UUID uuid){
        this.Uuid = uuid;
    }

    public BluetoothSocket getBluetoothSocket(){
        return socket;
    }

    public void setBluetoothSocket(BluetoothSocket socket){
        this.socket = socket;
    }

    public BluetoothService getBluetoothService() { return bluetoothService; }

    public void setBluetoothService(BluetoothService bluetoothService) { this.bluetoothService = bluetoothService; }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean getSendDataSetting() {
        return sendDataSetting;
    }

    public void setSendDataSetting(boolean b) {
        sendDataSetting = b;
    }

    public boolean getEmergencySetting() {
        return emergencySetting;
    }

    public void setEmergencySetting(boolean b) {
        emergencySetting = b;
    }

    public String getBufferBluetooth(){
        return bufferBluetooth;
    }

    public void setBufferBluetooth(String b){
        bufferBluetooth = b;
    }
}

