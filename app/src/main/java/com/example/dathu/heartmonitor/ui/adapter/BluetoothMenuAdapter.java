package com.example.dathu.heartmonitor.ui.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.ui.adapter.model.SettingItem;

import java.util.List;

public class BluetoothMenuAdapter extends RecyclerView.Adapter<BluetoothMenuAdapter.MyHolder>{
    private Context mContext;
    private List<BluetoothDevice> mItems;
    private OnItemClickListener mListener;

    public BluetoothMenuAdapter(Context context, List<BluetoothDevice> list){
        this.mContext = context;
        this.mItems = list;
    }
    @Override
    public BluetoothMenuAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_bluetooth_item, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(final BluetoothMenuAdapter.MyHolder holder, int position) {
        final int pos = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onClick(pos);
                }
            }
        });
        BluetoothDevice item = mItems.get(position);
        holder.imgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.bluetooth));
        holder.txtName.setText(item.getName() + "\n" + item.getAddress());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClickListener{
        void onClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    class MyHolder extends RecyclerView.ViewHolder{
        private ImageView imgIcon;
        private TextView txtName;
        MyHolder(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.img_icon);
            txtName = itemView.findViewById(R.id.txt_menu_name);
        }
    }
}
