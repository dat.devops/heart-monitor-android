package com.example.dathu.heartmonitor.ui;


import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.dathu.heartmonitor.HeartBeat;
import com.example.dathu.heartmonitor.MainApplication;
import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.bluetooth.BluetoothConnectionThread;
import com.example.dathu.heartmonitor.bluetooth.BluetoothService;
import com.example.dathu.heartmonitor.ui.adapter.BluetoothMenuAdapter;

import java.util.ArrayList;
import java.util.Set;

public class FragmentBluetooth extends Fragment{
    private RecyclerView rcvMenu;
    private BluetoothMenuAdapter adapter;
    private ArrayList<BluetoothDevice> listBluetooth;

    public static FragmentBluetooth newInstance(){
        return new FragmentBluetooth();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_setting, container, false);
        rcvMenu = v.findViewById(R.id.rcv_menu);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    private void initData(){
        listBluetooth = new ArrayList<>();
//        listSetting.add(new SettingItem(R.drawable.ic_ok, "Account Settings", 0));
//        listSetting.add(new SettingItem(R.drawable.ic_ok, "Bluetooth", 0));
//        listSetting.add(new SettingItem(R.drawable.ic_ok, "Log out", 0));
        discoverBluetooth();
   }

   private void discoverBluetooth(){
        Set<BluetoothDevice> pairedDevices = MainApplication.getInstance().getBluetoothAdapter().getBondedDevices();
        if (pairedDevices.size() > 0) {
            listBluetooth.addAll(pairedDevices);
        }
        adapter = new BluetoothMenuAdapter(getContext(), listBluetooth);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rcvMenu.setLayoutManager(layoutManager);
        rcvMenu.setAdapter(adapter);
   }

    private void initListener(){
        adapter.setOnItemClickListener(new BluetoothMenuAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                showToast("selected " + listBluetooth.get(position).getName());
                showLoadingDialog("Connecting to device...");
                new BluetoothConnectionThread(listBluetooth.get(position), new BluetoothConnectionThread.OnConnectListener() {
                    @Override
                    public void onConnect(BluetoothSocket socket) {
                        dismissLoading();
                        if(socket.isConnected()){
                            ((HeartBeat)getActivity()).loadHome();
                            MainApplication.getInstance().setBluetoothSocket(socket);
                        }else{
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showToast("Can't connect to device");
                                }
                            });
                        }
                    }
                }).start();
            }
        });
    }

    private void showToast(String toastMessage){
        Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT).show();
    }

    private AlertDialog loading;

    private void showLoadingDialog(String message){
        if(loading!=null){
            dismissLoading();
        }else{
            loading = new AlertDialog.Builder(getActivity())
                    .setTitle("Loading")
                    .setMessage(message)
                    .setCancelable(false)
                    .setIcon(getResources().getDrawable(R.drawable.circle_progress))
                    .create();
            loading.show();
        }
    }

    private void dismissLoading(){
        if(loading!=null && loading.isShowing()){
            loading.dismiss();
        }
    }
}
