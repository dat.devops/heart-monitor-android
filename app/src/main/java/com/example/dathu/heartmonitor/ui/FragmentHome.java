package com.example.dathu.heartmonitor.ui;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dathu.heartmonitor.HeartBeat;
import com.example.dathu.heartmonitor.MainApplication;
import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.bluetooth.BluetoothService;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Callback;

import static com.example.dathu.heartmonitor.Util.apiLog;
import static com.example.dathu.heartmonitor.Util.log;

public class FragmentHome extends Fragment implements View.OnClickListener {
    private static final int BPM = 0;
    private static final int RAW = 1;
    private static final int IBI = 2;

    private ImageButton btnOk;
    private TextView txtBpm, txtIbi;
    private LineChart mChart;
    //test
    public boolean isAdding = true;
    private Thread dynamicDataThread;
    private boolean top;

    public static FragmentHome newInstance() {
        return new FragmentHome();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        btnOk = v.findViewById(R.id.btn_ok);
        mChart = v.findViewById(R.id.chart);
        txtBpm = v.findViewById(R.id.txt_bpm);
        txtIbi = v.findViewById(R.id.txt_ibi);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initListener();
    }

    private void startPlotThread() {

        MainApplication.getInstance().setBluetoothService(new BluetoothService(MainApplication.getInstance().getBluetoothSocket(), new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message inputMessage) {
                String data = new String((byte[])inputMessage.obj);
                MainApplication.getInstance().setBufferBluetooth(data);

                if (((HeartBeat)getActivity()).isOnHome() && inputMessage.what == BluetoothService.MessageConstants.MESSAGE_READ) {
                    String[] lines = data.split("\n");
                    for (String line : lines) {
                        final String[] values = line.split(",");
                        if (values.length == 3 && !values[0].equals("") && !values[1].equals("") && !values[2].equals("")){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        onData(BPM, Integer.valueOf(values[0]));
                                        onData(RAW, Integer.valueOf(values[2]));
                                        onData(IBI, Integer.valueOf(values[1]));
                                        if (MainApplication.getInstance().getSendDataSetting()){
                                            MainApplication.getInstance().getFluxService().writeDB(
                                                     "Basic YWRtaW46MQ==",
                                                    RequestBody.create(MediaType.parse("text/plain"), "bpm,username=" + MainApplication.getInstance().getCurrentUser().userName + " value=" + values[0] + "\nibi,username=" + MainApplication.getInstance().getCurrentUser().userName + " value=" + values[1] + "\nraw,username=" + MainApplication.getInstance().getCurrentUser().userName + " value=" + values[2])
                                            ).enqueue(new retrofit2.Callback<Void>() {
                                                @Override
                                                public void onResponse(Call<Void> call, Response<Void> response) {
                                                    apiLog(new Gson().toJson(response));
                                                    if(response.code()==204){
                                                        showToast("success");
                                                    }else{
                                                        log(response.code()+"-"+response.message());
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<Void> call, Throwable t) {
                                                    log(t.getLocalizedMessage());
                                                    showToast(t.getMessage());
                                                }
                                            });
                                        }
                                    } catch (Exception e ) {
                                        e.printStackTrace();
                                    }
                              }
                            });
                        }

                    }
                }
            }
        }));
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPlot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MainApplication.getInstance().getBluetoothSocket()!= null && MainApplication.getInstance().getBluetoothSocket().isConnected()){
            startPlotThread();
        }
    }

    private void initData() {
        YAxis leftAxis = mChart.getAxisLeft();
        YAxis rightAxis = mChart.getAxisRight();
        XAxis xAxis = mChart.getXAxis();
        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);
        mChart.animateXY(1000, 1000);

        leftAxis.setEnabled(false);
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        rightAxis.setSpaceTop(15f);
        rightAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return "";
            }
        });
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
        rightAxis.setAxisMaximum(1000f);

        mChart.setData(new LineData());
        mChart.invalidate();
    }

    private int count = 0;

    private void onData(int chart, float value) {
        LineData data = mChart.getData();
        ILineDataSet set = data.getDataSetByIndex(chart);
        count++;
        if (chart == 0 && count == 500) {
            txtBpm.setText(String.valueOf(value));
            showToast("updated");
            count = 0;
        }
        if (chart == 2 && count == 500) {
            txtIbi.setText(String.valueOf(value));
            count = 0;
        }
        if (set == null) {
            String label;
            int color;
            switch (chart) {
                case 0:
                    label = "BPM";
                    color = Color.RED;
                    break;
                case 1:
                    color = Color.BLUE;
                    label = "Raw";
                    break;
                case 2:
                    color = Color.GREEN;
                    label = "IBI";
                    break;
                default:
                    color = Color.GRAY;
                    label = "HAHA";
                    break;
            }
            set = createSet(color, label);
            data.addDataSet(set);
            for (int i = 0; i < 25; i++) {
                set.addEntry(new Entry(i, 50));
            }
        }

        Entry entry = new Entry(set.getEntryCount(), value);
        data.addEntry(entry, chart);
        data.notifyDataChanged();

        mChart.notifyDataSetChanged();

        mChart.setVisibleXRangeMaximum(25);

//        Entry lastEntry = set.getEntryForIndex(set.getEntryCount() - 1);
        mChart.moveViewTo(entry.getX(), entry.getY(), YAxis.AxisDependency.LEFT);
    }

    public void stopPlot() {
//        dynamicDataThread.interrupt();
        if (MainApplication.getInstance().getBluetoothService() != null) {
            MainApplication.getInstance().getBluetoothService().cancel();
        }
    }

    private void initListener() {
        btnOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                if (isAdding) {
                    isAdding = false;
                } else {
                    if (dynamicDataThread != null && !dynamicDataThread.isAlive()) {
                        isAdding = true;
                        dynamicDataThread.run();
                    }
                }
                break;
        }
    }

    private LineDataSet createSet(int color, String label) {
        LineDataSet set = new LineDataSet(null, label);
        set.setDrawValues(false);
        set.setLineWidth(2.5f);
        set.setColor(color);
        set.setCircleColor(color);
        set.setDrawCircles(false);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        return set;
    }

    private void showToast(String toastMessage){
        Toast.makeText(getContext(), toastMessage, Toast.LENGTH_SHORT).show();
    }
}
