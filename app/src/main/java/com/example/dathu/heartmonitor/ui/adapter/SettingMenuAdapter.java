package com.example.dathu.heartmonitor.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dathu.heartmonitor.R;
import com.example.dathu.heartmonitor.ui.adapter.model.SettingItem;

import java.util.List;

public class SettingMenuAdapter extends RecyclerView.Adapter<SettingMenuAdapter.MyHolder>{
    private Context mContext;
    private List<SettingItem> mItems;
    private OnItemClickListener mListener;

    public SettingMenuAdapter(Context context, List<SettingItem> list){
        this.mContext = context;
        this.mItems = list;
    }
    @Override
    public SettingMenuAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.layout_setting_item, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(final SettingMenuAdapter.MyHolder holder, int position) {
        final int pos = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.onClick(pos);
                }
            }
        });
        SettingItem item = mItems.get(position);
        holder.imgIcon.setImageDrawable(mContext.getResources().getDrawable(item.getIcon()));
        if(item.getStatus()==1){
            holder.imgStatus.setVisibility(View.VISIBLE);
        }else{
            holder.imgStatus.setVisibility(View.GONE);
        }
        holder.txtName.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClickListener{
        void onClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public void setItemStatus(int position, int status){
        if(mItems!=null && !mItems.isEmpty() && position<getItemCount()){
            mItems.get(position).setStatus(status);
            notifyDataSetChanged();
        }
    }

    class MyHolder extends RecyclerView.ViewHolder{
        private ImageView imgIcon, imgStatus;
        private TextView txtName;
        MyHolder(View itemView) {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.img_icon);
            imgStatus = itemView.findViewById(R.id.img_status);
            txtName = itemView.findViewById(R.id.txt_menu_name);
        }
    }
}
