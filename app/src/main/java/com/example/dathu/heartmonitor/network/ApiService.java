package com.example.dathu.heartmonitor.network;

import com.example.dathu.heartmonitor.User;

import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @FormUrlEncoded
    @POST("user/login")
    Call<User> login(@Field("username") String username, @Field("password") String password);

    @POST("write?db=heartbeat")
    Call<Void> writeDB(@Header("Authorization") String header, @Body RequestBody requestBody);
}
