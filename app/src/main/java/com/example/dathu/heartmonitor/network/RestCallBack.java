package com.example.dathu.heartmonitor.network;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.dathu.heartmonitor.Util.apiLog;

public abstract class RestCallBack<T> implements Callback<T> {
    public abstract void success(@NonNull T body);
    public abstract void fail(@NonNull ApiError error);

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if(response.isSuccessful()){
            apiLog(new Gson().toJson(response.body()));
            T body = response.body();
            if(body!=null){
                success(body);
            }else{
                fail(new ApiError(response.code()+": "+response.message()));
            }
        }else{
            apiLog(new Gson().toJson(response));
            fail(new ApiError(response.code()+": "+response.message()));
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if(t instanceof IOException){
            if(t instanceof SocketTimeoutException){
                fail(new ApiError("Request timeout", t));
            }else{
                fail(new ApiError("No internet access", t));
            }
        }else{
            fail(new ApiError("Unknown error", t));
            apiLog(t.getLocalizedMessage());
        }
    }
}
