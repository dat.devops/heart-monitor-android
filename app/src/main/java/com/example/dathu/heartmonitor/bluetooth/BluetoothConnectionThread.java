package com.example.dathu.heartmonitor.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.dathu.heartmonitor.MainApplication;

import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Created by d40 on 12/14/17.
 */

public class BluetoothConnectionThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private OnConnectListener listener;

    public BluetoothConnectionThread(BluetoothDevice device, OnConnectListener listener) {
        this.listener = listener;
        BluetoothSocket tmp = null;
        mmDevice = device;
//        MainApplication.getInstance().setUuid(mmDevice.getUuids()[0].getUuid());
        try {
            tmp = device.createRfcommSocketToServiceRecord(MainApplication.getInstance().getUuid()); //Tao socket
        } catch (IOException e) {
            Log.e(TAG, "Socket's create() method failed", e);
        }
        mmSocket = tmp;
    }

    public void run() {
        // dam bao la bluetooth ko discover new device
        MainApplication.getInstance().getBluetoothAdapter().cancelDiscovery();

        try {
            mmSocket.connect();
        } catch (IOException connectException) {
            connectException.printStackTrace();
            try {
                mmSocket.close();
            } catch (IOException closeException) {
                Log.e(TAG, "Could not close the client socket", closeException);
            }
        }
        listener.onConnect(mmSocket);
        // Ket noi thanh cong, tao thread moi de handle data
//        BluetoothService bluetoothService = new BluetoothService(mmSocket);

    }

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e(TAG, "Could not close the client socket", e);
        }
    }

    public interface OnConnectListener{
        void onConnect(BluetoothSocket socket);
    }
}
