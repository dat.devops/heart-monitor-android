package com.example.dathu.heartmonitor;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dathu.heartmonitor.network.ApiError;
import com.example.dathu.heartmonitor.network.RestCallBack;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;
import static com.example.dathu.heartmonitor.Util.apiLog;
import static com.example.dathu.heartmonitor.Util.log;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION = 9999;
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        permission();
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        mEmailView.setText("dathuynh");
        mPasswordView.setText("123456");
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
//                makeCall();
//                testFlux();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void permission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.SEND_SMS
            }, REQUEST_PERMISSION);
        }
    }

    @SuppressLint("MissingPermission")
    private void makeCall(){
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage("+84987785395", null, "An cac ko dat?", null, null);
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:+84987785395"));
        startActivity(callIntent);
    }

    private void testFlux(){
        MainApplication.getInstance().getFluxService().writeDB(
                "Basic YWRtaW46MQ==",
                RequestBody.create(MediaType.parse("text/plain"), "bpm value=1234")
        ).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                apiLog(new Gson().toJson(response));
                if(response.code()==204){
                    showToast("success");
                }else{
                    log(response.code()+"-"+response.message());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                log(t.getLocalizedMessage());
                showToast(t.getMessage());
            }
        });
    }

    public void attemptLogin() {
        MainApplication.getInstance().getApiService().login(
                this.mEmailView.getText().toString().trim(),
                this.mPasswordView.getText().toString().trim()).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 200) {
                    MainApplication.getInstance().setCurrentUser(response.body());
                    Intent loggedIntent = new Intent(LoginActivity.this, HeartBeat.class);
                    startActivity(loggedIntent);
                    LoginActivity.this.finish();
                } else {
                    showToast("Authentication failed. Please re-check your username and password. Status: " + response.code());
                    Log.e("Login", new Gson().toJson(response));
                }
            }
            @Override public void onFailure(Call<User> call, Throwable t) {
                showToast("Please check your internet connection. Error: " + t.getMessage());
            }
        });
    }
    private void showToast(String toastMessage){
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
    }
}

