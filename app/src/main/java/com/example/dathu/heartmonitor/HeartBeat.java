package com.example.dathu.heartmonitor;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.dathu.heartmonitor.ui.FragmentHome;
import com.example.dathu.heartmonitor.ui.FragmentProfile;
import com.example.dathu.heartmonitor.ui.FragmentSetting;

public class HeartBeat extends AppCompatActivity {
    private FragmentHome home;
    private FragmentProfile profile;
    private FragmentSetting setting;

    private final int REQUEST_ENABLE_BT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart_beat);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
        loadHome();
        if (MainApplication.getInstance().getBluetoothAdapter() != null && !MainApplication.getInstance().getBluetoothAdapter().isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    public boolean isOnHome(){
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_content);
        return fragment instanceof FragmentHome;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_content);
            if(fragment instanceof FragmentHome){
                ((FragmentHome)fragment).stopPlot();
            }
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    loadHome();
                    return true;
                case R.id.navigation_dashboard:
                    loadProfile();
                    return true;
                case R.id.navigation_notifications:
                    loadSetting();
                    return true;
            }
            return false;
        }
    };

    public void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_content, fragment, null)
                .commit();
    }

    public void loadHome(){
        if(home==null)
            home = FragmentHome.newInstance();
        replaceFragment(home);
    }

    public void loadProfile(){
        if(profile==null)
            profile = FragmentProfile.newInstance();
        replaceFragment(profile);
    }

    public void loadSetting(){
        if(setting==null)
            setting = FragmentSetting.newInstance();
        replaceFragment(setting);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                MainApplication.getInstance().setBluetoothAdapter();
                showToast("Bluetooth Enabled");
            } else if (resultCode == RESULT_CANCELED) {
                showWarningDialog("You will not be able to monitor your heart beat if you don't enable Bluetooth",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
        }
    }

    private void showWarningDialog(String message, DialogInterface.OnClickListener okButtonEvent){
        new AlertDialog.Builder(this)
                .setTitle("Warning")
                .setMessage(message)
                .setPositiveButton("OK", okButtonEvent)
                .show();
    }

    private void showToast(String toastMessage){
        Toast.makeText(this,toastMessage, Toast.LENGTH_SHORT).show();
    }

    public void sendMessage() {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(MainApplication.getInstance().getCurrentUser().phone, null, "WARNING", null, null);
    }
}
