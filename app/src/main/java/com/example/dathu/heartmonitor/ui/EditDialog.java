package com.example.dathu.heartmonitor.ui;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.dathu.heartmonitor.R;


public class EditDialog extends Dialog {

    private EditText editText;
    private Button button;

    public EditDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.layout_dialog);
        Window window = getWindow();
        if(window!=null){
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        editText = findViewById(R.id.ed_content);
        button = findViewById(R.id.btn_ok);
    }

    public EditDialog setText(String text){
        editText.setText(text);
        return this;
    }

    public void show(final OnClickListener listener){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(editText.getText().toString());
                EditDialog.this.dismiss();
            }
        });
        this.show();
    }

    public interface OnClickListener{
        void onClick(String message);
    }
}
