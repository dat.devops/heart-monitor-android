package com.example.dathu.heartmonitor;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kienvanba on 12/15/17.
 */

public class User {
    @SerializedName("username")
    public String userName;
    @SerializedName("password")
    public String pwd;
    @SerializedName("name")
    public String name;
    @SerializedName("gender")
    public boolean isMale;
    @SerializedName("dob") // yyyy-MM-dd
    public String dob;
    @SerializedName("height")
    public int height;
    @SerializedName("weight")
    public int weight;
    @SerializedName("emergency_phone")
    public String phone;

    public User(String userName, String pwd, String name, boolean isMale, String dob, int height, int weight, String phone) {
        this.userName = userName;
        this.pwd = pwd;
        this.name = name;
        this.isMale = isMale;
        this.dob = dob;
        this.height = height;
        this.weight = weight;
        this.phone = phone;
    }
}