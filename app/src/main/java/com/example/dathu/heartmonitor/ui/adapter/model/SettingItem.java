package com.example.dathu.heartmonitor.ui.adapter.model;

public class SettingItem {
    private int icon;
    private String name;
    private int status;

    public SettingItem(int icon, String name, int status) {
        this.icon = icon;
        this.name = name;
        this.status = status;
    }

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
