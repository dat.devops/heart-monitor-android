package com.example.dathu.heartmonitor.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ApiError {
    private String errorMessage;
    private Throwable throwable;

    public ApiError(String message){
        this.errorMessage = message;
    }

    public  ApiError(String message, Throwable t){
        this.errorMessage = message;
        this.throwable = t;
    }

    @NonNull
    public String getErrorMessage(){
        return this.errorMessage;
    }

    @Nullable
    public Throwable getThrowable(){
        return this.throwable;
    }
}
